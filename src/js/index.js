'use strict';
// Retrieves cart html value and stores in a global variable
let counterItem = document.getElementById('counter-item').innerHTML;

//Method that increments the value in the cart as value of the parameter
function addToCart(value) { //
  value = (value + counterItem++);
  counterItem = value;

  document.getElementById('counter-item').innerHTML = value;
  return value;
}

//variable that stores vector figures for search
const figures = ['mario', 'luigi', 'bowser', 'peach', 'yoshi'];

//Method that clears the search field
function clearFields() {
  autocomplete.innerHTML = "";
  autocomplete.style.display = "none";
}

//Method that searches the typed term
function searchFigure() {
  if (!search.value) {
    clearFields();
    return;
  }
  let a = new RegExp("^" + search.value, "i");
  for (var x = 0, b = document.createDocumentFragment(), c = false; x < figures.length; x++) {
    if (a.test(figures[x])) {
      c = true;
      var d = document.createElement("p");
      d.innerText = figures[x];
      d.setAttribute(
        "onclick",
        "search.value=this.innerText;autocomplete.innerHTML='';autocomplete.style.display='none';");
      b.appendChild(d);
    }
  }
  if (c == true) {
    autocomplete.innerHTML = "";
    autocomplete.style.display = "block";
    autocomplete.appendChild(b);
    return;
  }
  clearFields();
}

search.addEventListener("keyup", searchFigure);
search.addEventListener("change", searchFigure);
search.addEventListener("focus", searchFigure);

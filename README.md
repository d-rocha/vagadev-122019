# The Front End Challenge by N1

## Introdução

# Avaliação Front-End N1

### O Básico a ser executado para concorrer a vaga

- Fork o repositório e inicie o desenvolvimento;
- Desenvolva o layout que está no arquivo .xd. O projeto deve ser responsivo. Lembrando que no layout só existe a versão desktop. Sendo assim adapte a responsividade como preferir.

Finalizando esses itens você terá terminado o level 01 da avaliação.

_OBS.: Interações e funcionalidades não sugeridas no layout serão levadas em consideração._

### Ultrapassando a concorrência

- Observe que no layout existe um modal(Quick cart) de sucesso da compra. Ele deve aparecer quando clicar no botão de comprar.
- Prever também a interação com o quick cart quando o usuário clicar no comprar do compre junto.

- **PLUS:** Junto com a ação de exibição do modal atualize a quantidade de itens no carrinho presente no header.

Finalizando esses itens você terá terminado o level 02 da avaliação.

### CHEFÃO para garantir a vaga:

- Desenvolver um autocomplete para busca no header.

## Tecnologias Utilizadas

- HTML 5
- CSS 3
- SASS
- Javascript (ES6)

# Para acessar o projeto:

Para execução deste projeto, clone este repositório e execute o arquivo index.html
na pasta /src
